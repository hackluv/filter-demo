<%@ page import="com.example.demo.entity.Member" %><%--
  Created by IntelliJ IDEA.
  User: hoang
  Date: 6/6/2019
  Time: 10:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% Member member = (Member) request.getAttribute("member");
    if (member == null) {
        member = new Member();
    }%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/demo_zzz_war_exploded/member/register" method="post">
    <h1>Register</h1>
    <input placeholder="Username" name="username" type="text">
    <input placeholder="Full Name" name="fullName" type="text" >
    <input type="email" placeholder="Email" name="email" >
    <input type="password" placeholder="Password" name="password">
    <input type="password" placeholder="confirm Password" name="con_password">
    <input type="text" placeholder="Phone" name="phone">
    <div>
        <select name="gender" title="Gender">
            <option value="0">Female</option>
            <option value="1">Male</option>
            <option value="2">Others</option>
        </select>
    </div>

    <div>
        <select name="role" title="Role">
            <option value="0">Member</option>
            <option value="1">Admin</option>
        </select>
    </div>
    <button>Register</button>
</form>
</body>
<style>
    * {
        font-family: 'Maven Pro', sans-serif;
        box-sizing: border-box;
    }

    body, html {
        height: 100%;
        width: 100%;
        margin: 0;
        padding: 0;
        background-image: linear-gradient(180deg, #37375A 70%, #31315A);
        text-align: center;
        font-family: 'Segoe UI';
    }

    form {
        width: 40%;
        margin-left: 30%;
        padding-top: 10%;
    }

    input {
        width: 100%;
        background: transparent;
        border-bottom: solid 1px #7053c4;
        border-top: none;
        border-left: none;
        border-right: none;
        font-size: 1rem;
        padding: 0.5em 0.4em;
        transition: all 0.4s;
        color: #BDBDBD;;
        margin: 0.7rem 0;
    }

    input:focus {
        background: #7035c4;
        transform: scale3d(1.06, 1.06, 1.06);
    }

    button {
        background: transparent;
        width: 50%;
        margin-top: 2.5rem;
        font-size: 1rem;
        border: solid 1px #7053c4;
        padding: 1em 0;
        color: #bdc3c7;
        transition: all 0.6s;
    }

    button:hover {
        cursor: pointer;
        background: #7035c4;
    }

    h1 {
        color: #bdc3c7;
        border-bottom: solid 1px #7035c4;
        padding: 0 0 0.8em 0;
        width: 50%;
        margin-left: 25%;
        margin-bottom: 1em;
    }

    @media (max-width: 550px) {
        form {
            width: 90%;
            margin-left: 3%;
            padding-top: 5%;
        }

        input {
            font-size: 1em;
        }
    }
</style>
</html>


