package com.example.demo.controller;

import com.example.demo.entity.Member;
import com.example.demo.model.MemberModel;
import com.example.demo.util.Generate;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RegisterController extends HttpServlet {
    MemberModel model = new MemberModel();
    Generate generate = new Generate();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Member member = new Member();
        req.setAttribute("member", member);
        req.getRequestDispatcher("/member/register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Gson gson = new Gson();
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String fullName = req.getParameter("fullName");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        int role = Integer.parseInt(req.getParameter("role"));
        int gender = Integer.parseInt(req.getParameter("gender"));
        String id = "Member" + generate.gen7Digits();
        String salt = generate.getSalt(10);
        String pwdCompare = password+salt;

        Member member = new Member();
        member.setId(id);
        member.setUsername(username);
        member.setPassword(generate.md5(pwdCompare));
        member.setSalt(salt);
        member.setFullname(fullName);
        member.setEmail(email);
        member.setPhone(phone);
        member.setGender(Member.Gender.findByValue(gender));
        member.setRole(Member.Role.findByValue(role));

        try {
            model.register(member);
        } catch (Exception e) {
            e.printStackTrace();
        }

        resp.getWriter().println(gson.toJson(member));
    }
}
