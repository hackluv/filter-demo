package com.example.demo.controller;

import com.example.demo.entity.Member;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class AdminController extends HttpServlet {
    HttpSession session;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Gson gson = new Gson();
        session = req.getSession();
        Member member = (Member) session;
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        out.print(gson.toJson(member));
        out.close();
    }
}
