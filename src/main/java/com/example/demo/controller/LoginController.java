package com.example.demo.controller;

import com.example.demo.entity.Member;
import com.example.demo.model.MemberModel;
import com.example.demo.util.Generate;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

public class LoginController extends HttpServlet {
    Generate generate = new Generate();
    MemberModel model = new MemberModel();
    HttpSession session;
    Member member = new Member();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie :
                    cookies) {
                System.out.println(cookie.getName() + " - " + cookie.getValue() + " - " + cookie.getDomain());
            }
        }
        session = req.getSession();
        Member member = (Member) session.getAttribute("currentLoggedIn");
        req.setAttribute("member", member);
        req.getRequestDispatcher("/member/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        req.setCharacterEncoding("UTF-8");

        String username = req.getParameter("email");
        String password = req.getParameter("password");
        try {
            member = model.login(username, Member.Status.ACTIVE);
            if (member == null) {
                resp.setStatus(404);
                resp.getWriter().println("Email not exist or Delete!!");
            } else {
                // Encrypt ( Password + salt ) and compare with Database Password
                String hashPass = generate.md5(password + member.getSalt());
                if (hashPass.equals(member.getPassword())) {
                    session = req.getSession();
                    session.setAttribute("currentLoggedIn", member);
                    resp.setStatus(200);
                    resp.getWriter().println("Login Success");
                } else {
                    resp.setStatus(401);
                    resp.getWriter().println("Something wrong.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        resp.getWriter().println("Login Success with account: " + member.getEmail());
    }
}
