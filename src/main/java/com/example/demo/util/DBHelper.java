package com.example.demo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBHelper {
    Connection connection;
    private final String CONNECT_URL = "jdbc:mysql://localhost:3306/";
    private final String DATABASE = "assignment";
    private final String USERNAME = "root";
    private final String PASSWORD = "";
    private final String UTF8_URL = "?useUnicode=true&characterEncoding=utf-8";
    private final String CLASS_FORNAME = "com.mysql.jdbc.Driver";
    private static DBHelper instance;

    public static DBHelper getInstance() {
        if (instance == null) {
            instance = new DBHelper();
        }
        return instance;
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        if (connection == null || connection.isClosed()) {
            Class.forName(CLASS_FORNAME);
            connection = DriverManager.getConnection(CONNECT_URL + DATABASE + UTF8_URL, USERNAME, PASSWORD);
            System.out.println("Okie");
        }
        return connection;
    }
}
