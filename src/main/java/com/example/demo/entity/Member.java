package com.example.demo.entity;

public class Member {
    private String id;
    private String username;
    private String password;
    private String salt;
    private String fullname;
    private String email;
    private String phone;
    private int gender;
    private int status;
    private int role;

    public enum Gender {
        MALE(1), FEMALE(0), OTHERS(2);
        int value;

        Gender(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Gender findByValue(int value) {
            for (Gender gender :
                    Gender.values()) {
                if (gender.getValue() == value) {
                    return gender;
                }
            }
            return null;
        }
    }

    public enum Status {
        ACTIVE(1), DEACTIVE(0), DELETED(-1);

        int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status findByValue(int value) {
            for (Status status :
                    Status.values()) {
                if (status.getValue() == value){
                    return status;
                }
            }
            return null;
        }
    }

    public enum Role {
        ADMIN(1), MEMBER(0);

        int value;

        Role(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Role findByValue(int value) {
            for (Role role :
                    Role.values()) {
                if (role.getValue() == value){
                    return role;
                }
            }
            return null;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        if (gender == null){
            gender = Gender.FEMALE;
        }
        this.gender = gender.getValue();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        if (status == null){
            status = Status.DEACTIVE;
        }
        this.status = status.getValue();
    }

    public int getRole() {
        return role;
    }

    public void setRole(Role role) {
        if (role == null){
            role = Role.MEMBER;
        }
        this.role = role.getValue();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
