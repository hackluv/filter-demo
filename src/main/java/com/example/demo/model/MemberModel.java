package com.example.demo.model;

import com.example.demo.entity.Member;
import com.example.demo.util.DBHelper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MemberModel {
    DBHelper db = new DBHelper();
    Connection cnn;

    public boolean register(Member member) throws SQLException, ClassNotFoundException {
        cnn = db.getConnection();
        String query = "INSERT INTO member (id, username, password, salt, fullname, email, phone, gender, status, role) values (?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = cnn.prepareStatement(query);
        preparedStatement.setString(1, member.getId());
        preparedStatement.setString(2, member.getUsername());
        preparedStatement.setString(3, member.getPassword());
        preparedStatement.setString(4, member.getSalt());
        preparedStatement.setString(5, member.getFullname());
        preparedStatement.setString(6, member.getEmail());
        preparedStatement.setString(7, member.getPhone());
        preparedStatement.setInt(8, member.getGender());
        preparedStatement.setInt(9, member.getStatus());
        preparedStatement.setInt(10, member.getRole());
        preparedStatement.execute();
        return true;
    }

    public Member login(String email, Member.Status status) throws SQLException, ClassNotFoundException {
        Member member = new Member();
        cnn = db.getConnection();
        String sqlCommand = "SELECT * FROM member WHERE email = ? AND status = ?";
        PreparedStatement preparedStatement = cnn.prepareStatement(sqlCommand);
        preparedStatement.setString(1, email);
        preparedStatement.setInt(2, status.getValue());
        ResultSet result = preparedStatement.executeQuery();

        if (result.next()){
            String id = result.getString("id");
            String username = result.getString("username");
            String password1 = result.getString("password");
            String salt = result.getString("salt");
            String phone = result.getString("phone");
            String fullName = result.getString("fullName");
            String email1 = result.getString("email");
            int status1 = result.getInt("status");
            int role = result.getInt("role");
            int gender = result.getInt("gender");

            member.setId(id);
            member.setPhone(phone);
            member.setEmail(email1);
            member.setPassword(password1);
            member.setSalt(salt);
            member.setFullname(fullName);
            member.setUsername(username);
            member.setStatus(Member.Status.findByValue(status1));
            member.setGender(Member.Gender.findByValue(gender));
            member.setRole(Member.Role.findByValue(role));
        }
        return member;
    }
}
