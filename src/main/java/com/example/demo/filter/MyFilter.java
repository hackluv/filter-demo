package com.example.demo.filter;

import com.example.demo.entity.Member;
import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MyFilter implements Filter {
    HttpSession session;
    Gson gson = new Gson();
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        session = request.getSession();
        if (session != null){
            Member member = (Member) session.getAttribute("currentLoggedIn");
            if (member.getRole() == 0){
                filterChain.doFilter(request,response);
            }else if (member.getRole() == 1){
//                RequestDispatcher rd = request.getRequestDispatcher("/member/welcome.jsp");
//                rd.include(request,response);
                response.getWriter().println(gson.toJson(member));
            }
        }

    }

    public void destroy() {

    }
}
